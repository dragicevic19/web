package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.StringTokenizer;

import beans.Pacijent;

/***
 * Klasa namenjena da u�ita proizvode iz fajla i pru�a operacije nad njima
 * (poput pretrage). Proizvodi se nalaze u fajlu WebContent/products.txt u
 * obliku: <br>
 * id;naziv;jedinicna cena
 * 
 * @author Lazar
 *
 */
public class PacijentDAO {

	private HashMap<String, Pacijent> pacijenti = new HashMap<String, Pacijent>();

	public PacijentDAO() {

	}

	/***
	 * @param contextPath Putanja do aplikacije u Tomcatu. Mo�e se pristupiti samo
	 *                    iz servleta.
	 */
	public PacijentDAO(String contextPath) {
		//loadProducts(contextPath);
	}

	/***
	 * Vra�a sve proizvode.
	 * 
	 * @return
	 */
	public Collection<Pacijent> findAll() {
		return pacijenti.values();
	}
	
	public void addPacijent(String brOsig, Pacijent p) {
		pacijenti.put(brOsig, p);
	}

	/***
	 * Vraca proizvod na osnovu njegovog id-a.
	 * 
	 * @return Proizvod sa id-em ako postoji, u suprotnom null
	 */
	public Pacijent findPacijent(String brOsiguranja) {
		return pacijenti.containsKey(brOsiguranja) ? pacijenti.get(brOsiguranja) : null;
	}

	/**
	 * U�itava korisnike iz WebContent/pacijenti.txt fajla i dodaje ih u mapu
	 * {@link #products}. Klju� je id proizovda.
	 * 
	 * @param contextPath Putanja do aplikacije u Tomcatu
	 */
/*	private void loadProducts(String contextPath) {
		BufferedReader in = null;
		try {
			File file = new File(contextPath + "/products.txt");
			System.out.println(file.getCanonicalPath());
			in = new BufferedReader(new FileReader(file));
			String line, id = "", name = "", price = "";
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0)
					continue;
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					id = st.nextToken().trim();
					name = st.nextToken().trim();
					price = st.nextToken().trim();
				}
				products.put(id, new Product(id, name, Double.parseDouble(price)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
				}
			}
		}

	}
*/
}
