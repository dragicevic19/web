package beans;

public enum ZdravStatus {
	BEZ_SIMPTOMA,
	SA_SIMPTOMIMA,
	ZARAZEN,
	IZLECEN
}
