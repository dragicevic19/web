package beans;

public class Pacijent {

	private String brOsiguranja;
	private String ime;
	private String prezime;
	private String datumRodjenja;
	private Pol pol;
	private ZdravStatus zdravStatus;

	public Pacijent() {
	}

	public Pacijent(String brOsiguranja, String ime, String prezime, String datumRodjenja, Pol pol,
			ZdravStatus zdravStatus) {
		super();
		this.brOsiguranja = brOsiguranja;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = datumRodjenja;
		this.pol = pol;
		this.zdravStatus = zdravStatus;
	}

	public String getBrOsiguranja() {
		return brOsiguranja;
	}

	public void setBrOsiguranja(String brOsiguranja) {
		this.brOsiguranja = brOsiguranja;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(String datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public Pol getPol() {
		return pol;
	}

	public void setPol(Pol pol) {
		this.pol = pol;
	}

	public ZdravStatus getZdravStatus() {
		return zdravStatus;
	}

	public void setZdravStatus(ZdravStatus zdravStatus) {
		this.zdravStatus = zdravStatus;
	}

	@Override
	public String toString() {
		return "Pacijent [brOsiguranja=" + brOsiguranja + ", ime=" + ime + ", prezime=" + prezime + ", datumRodjenja="
				+ datumRodjenja + ", pol=" + pol + ", zdravStatus=" + zdravStatus + "]";
	}

}
