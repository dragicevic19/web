package services;

import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import beans.Stan;
import dao.StanoviDAO;

@Path("/stanovi")
public class StanoviService {
	@Context
	ServletContext ctx;

	public StanoviService() {
	}

	@PostConstruct
	// ctx polje je null u konstruktoru, mora se pozvati nakon konstruktora
	// (@PostConstruct anotacija)
	public void init() {
		// Ovaj objekat se instancira vi�e puta u toku rada aplikacije
		// Inicijalizacija treba da se obavi samo jednom
		if (ctx.getAttribute("stanoviDAO") == null) {
			String contextPath = ctx.getRealPath("");
			ctx.setAttribute("stanoviDAO", new StanoviDAO(contextPath));
		}
	}

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Stan> getStanovi() {
		StanoviDAO dao = (StanoviDAO) ctx.getAttribute("stanoviDAO");
		return dao.findAll();
	}

	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Stan newStan(Stan stan) {
		StanoviDAO dao = (StanoviDAO) ctx.getAttribute("stanoviDAO");
		return dao.save(stan.getBrStana(), stan);
	}
	
//	@GET
//	@Path("/pretraga}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Collection<Stan> pretragaStanova(@QueryParam("kvOd") String kvOd,
//			@QueryParam("kvDo") String kvDo, @QueryParam("cg") boolean cg){
//		
//		// pretraga
//	}
}
