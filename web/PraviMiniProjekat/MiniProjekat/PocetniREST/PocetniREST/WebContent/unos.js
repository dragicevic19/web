function addStanTr(stan) {
    let tr = (stan.brKvadrata > 50) ? $('<tr style="background-color:lightgrey"></tr>') : $('<tr></tr>');
	let tdBrStana = $('<td>' + stan.brStana + '</td>');
    let tdAdresa = $('<td>' + stan.adresa + '</td>');
    let tdGrad = $('<td>' + stan.grad + '</td>');
    let tdBrSoba = $('<td>' + stan.brSoba + '</td>');
    let tdBrKvadrata = $('<td>' + stan.brKvadrata + '</td>');
    let tdCentralno = (stan.centralnoGrejanje) ? $('<td>DA</td>') : $('<td>NE</td>');
    let tdCena = $('<td>' + stan.cena + '</td>');
    let tdDostupan = (!stan.dostupan) ? $('<td><a href="#" class="link">Dostupan</a>') : $('<td></td>');
    
    tr.append(tdBrStana).append(tdAdresa).append(tdGrad).append(tdBrSoba).append(tdBrKvadrata)
        .append(tdCentralno).append(tdCena).append(tdDostupan);
    
	$('#tabelaPregled tbody').append(tr);
}

$(document).ready(function () {	
    $.get({
		url: 'rest/stanovi/',
		success: function(stanovi) {
			for (let stan of stanovi) {
				addStanTr(stan);
			}
		}
	});
    
    $('#formUnos').submit(function (event) {
        event.preventDefault();
        let brStan = $('input[name="brStana"]').val();
        let adresa = $('input[name="adresa"]').val();
        let grad = $('input[name="grad"]').val();
        let brSoba = $('input[name="brSoba"]').val();
        let brKvadrata = $('input[name="brKvadrata"]').val();
        let centralno = $('input[name="centralno"]').is(":checked"); 
        let cena = $('input[name="cena"]').val();
        
        $.post({
			url: 'rest/stanovi/',
            data: JSON.stringify(
                {
                    brStana: brStan,
                    adresa: adresa,
                    grad: grad,
                    brSoba: brSoba,
                    brKvadrata: brKvadrata,
                    centralnoGrejanje: centralno,
                    cena: cena,
                    dostupan: false
                }),
			contentType: 'application/json',
			success: function(stan) {
				$('#success').text('Novi proizvod uspešno kreiran.');
				$("#success").show().delay(3000).fadeOut();
				// Dodaj novi proizvod u tabelu
				addStanTr(stan);
			}
		});
    });

/*	$('#formPretraga').submit(function (event) {
		event.preventDefault();
        let kvOd = $('input[name="kvOd"]').val();
        let kvDo = $('input[name="kvDo"]').val();
        let cg = $('input[name="cg"]').is(":checked");

		$.get()
	});*/
	
    $("a").click(function (event) {
        event.preventDefault();
        let red = $(this).parent().parent();
        alert(red.find('td:eq(0)').text());

    });
        
});