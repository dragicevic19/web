package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Stan;
import dao.StanoviDAO;

/**
 * Servlet implementation class Pretraga
 */
@WebServlet("/Pretraga")
public class Pretraga extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Pretraga() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String adresa = request.getParameter("adresa");
		
		ServletContext context = getServletContext();
		StanoviDAO stanovi = (StanoviDAO) context.getAttribute("stanovi");
		StanoviDAO stanoviZaPrikaz = new StanoviDAO();
		
		for (Stan stan : stanovi.findAll()) {
			if (stan.getAdresa().contains(adresa)) {
				stanoviZaPrikaz.addStan(stan.getBrStana(), stan);
			}
		}
		
		HttpSession session = request.getSession();

		if (stanoviZaPrikaz.findAll().size() == 0) {
			session.setAttribute("errPretraga", "Ne postoji stan sa trazenim kriterijumom");
		}
		else {
			session.setAttribute("stanoviZaPrikaz", stanoviZaPrikaz);
	    	session.setAttribute("prikazPretrage", false);
			session.setAttribute("errPretraga", "");
		}
		response.sendRedirect("JSP/stanovi.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
