package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Stan;
import dao.StanoviDAO;

/**
 * Servlet implementation class DodajStan
 */
@WebServlet("/DodajStan")
public class DodajStan extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DodajStan() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	super.init();
    	ServletContext context = getServletContext();
    	String contextPath = context.getRealPath("");
    	// Dodaju se korisnici u kontekst kako bi mogli servleti da rade sa njima
    	context.setAttribute("stanovi", new StanoviDAO(contextPath));
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher disp = request.getRequestDispatcher("/JSP/unos.jsp");
    	disp.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletContext context = getServletContext();
    	StanoviDAO stanovi = (StanoviDAO) context.getAttribute("stanovi");
    	
    	String brStana = request.getParameter("brStana");
    	Stan s = stanovi.findStan(brStana);
		HttpSession session = request.getSession();

    	if (s != null) {
    		session.setAttribute("err", "Stan vec kreiran!");
    		response.sendRedirect("DodajStan");
    		return;
    	}
    	
    	session.removeAttribute("err");
    	String adresa = request.getParameter("adresa");
    	String grad = request.getParameter("grad");
    	int brSoba = Integer.parseInt(request.getParameter("brSoba"));
    	int brKvadrata = Integer.parseInt(request.getParameter("brKvadrata"));
    	boolean centralno = false;
    	String cGrejanje = request.getParameter("centralno");
    	if (cGrejanje != null) {
    		if(cGrejanje.equals("on")) {
    			centralno = true;
    		}
    	}
    	double cena = Double.parseDouble(request.getParameter("cena"));
    	
    	Stan stan = new Stan(brStana, adresa, grad, brSoba, brKvadrata, centralno, cena);
    	stanovi.addStan(brStana, stan);
    	
    	session.setAttribute("stanoviZaPrikaz", stanovi);
    	session.setAttribute("prikazPretrage", true);
		session.setAttribute("errPretraga", "");

    	response.sendRedirect("JSP/stanovi.jsp");
	}

}
