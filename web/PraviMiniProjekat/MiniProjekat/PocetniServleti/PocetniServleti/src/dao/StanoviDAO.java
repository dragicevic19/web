package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.StringTokenizer;

import beans.Stan;

/***
 * Klasa namenjena da u�ita proizvode iz fajla i pru�a operacije nad njima
 * (poput pretrage). Proizvodi se nalaze u fajlu WebContent/products.txt u
 * obliku: <br>
 * id;naziv;jedinicna cena
 * 
 * @author Lazar
 *
 */
public class StanoviDAO {

	private HashMap<String, Stan> stanovi = new HashMap<String, Stan>();

	public StanoviDAO() {

	}

	/***
	 * @param contextPath Putanja do aplikacije u Tomcatu. Mo�e se pristupiti samo
	 *                    iz servleta.
	 */
	public StanoviDAO(String contextPath) {
		loadProducts(contextPath);
	}

	/***
	 * Vra�a sve proizvode.
	 * 
	 * @return
	 */
	public Collection<Stan> findAll() {
		return stanovi.values();
	}
	
	public void addStan(String brStana, Stan s) {
		stanovi.put(brStana, s);
	}

	/***
	 * Vraca proizvod na osnovu njegovog id-a.
	 * 
	 * @return Proizvod sa id-em ako postoji, u suprotnom null
	 */
	public Stan findStan(String brStana) {
		return stanovi.containsKey(brStana) ? stanovi.get(brStana) : null;
	}

	/**
	 * U�itava korisnike iz WebContent/pacijenti.txt fajla i dodaje ih u mapu
	 * {@link #products}. Klju� je id proizovda.
	 * 
	 * @param contextPath Putanja do aplikacije u Tomcatu
	 */
	private void loadProducts(String contextPath) {
		BufferedReader in = null;
		try {
			File file = new File(contextPath + "/stanovi.txt");
			System.out.println(file.getCanonicalPath());
			in = new BufferedReader(new FileReader(file));
			String line, brStana = "", adresa = "", grad = "", brSoba="", brKvadrata="", centralno="", cena="", dostupno="";
			StringTokenizer st;
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0)
					continue;
				st = new StringTokenizer(line, ";");
				while (st.hasMoreTokens()) {
					brStana = st.nextToken().trim();
					adresa = st.nextToken().trim();
					grad = st.nextToken().trim();
					brSoba = st.nextToken().trim();
					brKvadrata = st.nextToken().trim();
					centralno = st.nextToken().trim();
					cena = st.nextToken().trim();
					dostupno = st.nextToken().trim();
				}
				stanovi.put(brStana, new Stan(brStana, adresa, grad, Integer.parseInt(brSoba), Integer.parseInt(brKvadrata),
						Boolean.parseBoolean(centralno), Double.parseDouble(cena), Boolean.parseBoolean(dostupno)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
				}
			}
		}

	}

}
