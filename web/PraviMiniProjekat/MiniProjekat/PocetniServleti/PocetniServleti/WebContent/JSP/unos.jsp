<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSP unos stanova</title>
</head>
<body>
	<h1 style="color: red;">JSP.Evidencija stanova</h1>
		<form action="DodajStan" method="POST" style="align-items: center;">
			<table>
				<tr>
					<td><b>Broj stana(id):</b></td><td><input type="text" name="brStana"/></td>
				</tr>
				<tr>
					<td><b>Adresa:</td><td><input type="text" name="adresa"/></td>
				</tr>
				<tr>
					<td><b>Grad:</td><td><input type="text" name="grad"/></td>
				</tr>
				<tr>
					<td><b>Broj soba:</td><td><input type="text" name="brSoba"/></td>
				</tr>
				<tr>
					<td><b>Broj kvadrata:</td><td><input type="text" name="brKvadrata"/></td>
				</tr>	
				<tr>
					<td><b>Centralno grejanje:</b></td><td><input type="checkbox" name="centralno"/></td>
				</tr>
				<tr>
					<td><b>Cena:</td><td><input type="text" name="cena"/></td>
				</tr>	
				<tr>
					<td></td><td><input type="submit" value="Posalji"></td>
				</tr>
			</table>
		</form>
		<!-- Prikaži grešku, ako je bilo -->
		<c:if test="${not empty err}">
			<p style="color:red">${err}</p>
		</c:if>
</body>
</html>