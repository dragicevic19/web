<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="beans.Stan"%>
<jsp:useBean id="stanovi" class="dao.StanoviDAO" scope="application"/>
<jsp:useBean id="stanoviZaPrikaz" class="dao.StanoviDAO" scope="session"/>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Pregled stanova</title>
</head>
<body>
	<h2 style="color: red;">JSP.Pregled stanova</h2>
	
	<table border='1px'>
		<tr>
			<th><b>Broj stana</b></th>
			<th><b>Adresa</b></th>
			<th><b>Grad</b></th>
			<th><b>Broj soba</b></th>
			<th><b>Broj kvadrata</b></th>
			<th><b>Centralno grejanje</b></th>
			<th><b>Cena</b></th>
			<th></th>
		</tr>
		<c:forEach var="stan" items="${stanoviZaPrikaz.findAll()}">
			<c:choose>
				<c:when test="${stan.dostupan == true}">
					<tr style="background-color:lightgrey;">
						<td>${stan.brStana}</td>
						<td>${stan.adresa}</td>
						<td>${stan.grad}</td>
						<td>${stan.brSoba}</td>
						<td>${stan.brKvadrata}</td>
						<td>${stan.centralnoGrejanje}</td>
						<td>${stan.cena}</td>
						<td>
							<c:choose>
								<c:when test="${stan.dostupan == false}">
									<a href="../Dostupan?brStana=${stan.brStana}">Dostupan</a>
								</c:when>	
								<c:otherwise></c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:when>
				<c:otherwise>
					<tr>
						<td>${stan.brStana}</td>
						<td>${stan.adresa}</td>
						<td>${stan.grad}</td>
						<td>${stan.brSoba}</td>
						<td>${stan.brKvadrata}</td>
						<td>${stan.centralnoGrejanje}</td>
						<td>${stan.cena}</td>
						<td>
							<c:choose>
								<c:when test="${stan.dostupan == false}">
									<a href="../Dostupan?brStana=${stan.brStana}">Dostupan</a>
								</c:when>	
								<c:otherwise></c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</c:forEach>	
	</table>
	
	<br />
	
	<c:if test="${prikazPretrage == true}">
		<form action="../Pretraga" method="GET">
			Adresa: <input type="text" name="adresa"><br>
			<input type="submit" value="Pretrazi"/>
		</form>
	</c:if>
	
	<!-- Prikaži grešku, ako je bilo -->
	<c:if test="${not empty errPretraga}">
		<p style="color:red">${errPretraga}</p>
	</c:if>
	
	<a href="../DodajStan">Nazad na unos stanova</a>
</body>
</html>