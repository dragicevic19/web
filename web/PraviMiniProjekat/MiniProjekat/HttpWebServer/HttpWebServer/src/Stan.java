
public class Stan {
	
	private String brStana;
	private String adresa;
	private String grad;
	private int brSoba;
	private int brKvadrata;
	private boolean centralnoGrejanje;
	private double cena;
	private boolean dostupan;
	
	public Stan() {}
	
	public Stan(String brStana, String adresa, String grad, int brSoba, int brKvadrata, boolean centralnoGrejanje,
			double cena) {
		super();
		this.brStana = brStana;
		this.adresa = adresa;
		this.grad = grad;
		this.brSoba = brSoba;
		this.brKvadrata = brKvadrata;
		this.centralnoGrejanje = centralnoGrejanje;
		this.cena = cena;
		this.dostupan = false;
	}
	
	public Stan(String brStana, String adresa, String grad, int brSoba, int brKvadrata, boolean centralnoGrejanje,
			double cena, boolean dostupan) {
		super();
		this.brStana = brStana;
		this.adresa = adresa;
		this.grad = grad;
		this.brSoba = brSoba;
		this.brKvadrata = brKvadrata;
		this.centralnoGrejanje = centralnoGrejanje;
		this.cena = cena;
		this.dostupan = dostupan;
	}

	public boolean isDostupan() {
		return dostupan;
	}
	
	public void setDostupan(boolean d) {
		this.dostupan = d;
	}
	
	public String getBrStana() {
		return brStana;
	}
	public void setBrStana(String brStana) {
		this.brStana = brStana;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public String getGrad() {
		return grad;
	}
	public void setGrad(String grad) {
		this.grad = grad;
	}
	public int getBrSoba() {
		return brSoba;
	}
	public void setBrSoba(int brSoba) {
		this.brSoba = brSoba;
	}
	public int getBrKvadrata() {
		return brKvadrata;
	}
	public void setBrKvadrata(int brKvadrata) {
		this.brKvadrata = brKvadrata;
	}
	public boolean isCentralnoGrejanje() {
		return centralnoGrejanje;
	}
	public void setCentralnoGrejanje(boolean centralnoGrejanje) {
		this.centralnoGrejanje = centralnoGrejanje;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	
	

}
