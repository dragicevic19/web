import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server
{
    private int port;
    private ServerSocket serverSocket;
    private List<Stan> stanovi; 


    public Server(int port)
            throws IOException
    {
        this.port = port;
        this.serverSocket = new ServerSocket(this.port);
		this.stanovi = new ArrayList<Stan>();

    }


    public void run()
    {
        System.out.println("Web server running on port: " + port);
        System.out.println("Document root is: " + new File("/static").getAbsolutePath() + "\n");

        Socket socket;

        while (true)
        {
            try
            {
                // prihvataj zahteve
                socket = serverSocket.accept();
                InetAddress addr = socket.getInetAddress();

                // dobavi resurs zahteva
                String resource = this.getResource(socket.getInputStream());
                
                // fail-safe
                if (resource == null)
                    continue;

                if (resource.equals(""))
                    resource = "static/index.html";

                System.out.println("Request from " + addr.getHostName() + ": " +  resource);

                // posalji odgovor
                this.sendResponse(resource, socket.getOutputStream());
                socket.close();
                socket = null;
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }


    private String getResource(InputStream is)
            throws IOException
    {
        BufferedReader dis = new BufferedReader(new InputStreamReader(is));
        String s = dis.readLine();

        // fail-safe
        if (s == null)
            return null;

        String[] tokens = s.split(" ");

        // prva linija HTTP zahteva: METOD /resurs HTTP/verzija
        // obradjujemo samo GET metodu
        String method = tokens[0];
        if (!method.equals("GET"))
            return null;

        // String resursa
        String resource = tokens[1];

        // izbacimo znak '/' sa pocetka
        resource = resource.substring(1);

        // ignorisemo ostatak zaglavlja
        String s1;
        while (!(s1 = dis.readLine()).equals(""))
            System.out.println(s1);

        return resource;
    }


    private void sendResponse(String resource, OutputStream os)
            throws IOException
    {
        PrintStream ps = new PrintStream(os);

        // zamenimo web separator sistemskim separatorom
        // resource = resource.replace('/', File.separatorChar);
        
        String resources[] = resource.split("[?]");
		String function = resources[0];

		if (function.startsWith("dodaj")) {
			String response = dodaj(resources[1]);

			if (response == null) {
				response = "HTTP/1.0 200 OK\n" + "Content-type: text/html; charset=UTF-8\r\n\r\n"
						+ "<html><head><title>Prijavljeni pacijenti</title><body>Stan vec kreiran!"
						+ "<br><a href=\"http://localhost:8000/\">Nazad</a></body></head>";
				ps.print(response);
				ps.flush();
				return;
			}

			ps.print(response);
			ps.flush();
			return;
		}
		else if (function.startsWith("pretraga")) {
			String response = pretraga(resources[1]);

			if (response == null) {
				response = "HTTP/1.0 200 OK\n" + "Content-type: text/html; charset=UTF-8\r\n\r\n"
						+ "<html><head><title>Prijavljeni pacijenti</title><body>Ne postoji stan za trazeni grad!"
						+ "<br><a href=\"http://localhost:8000/\">Nazad na prijavu pacijenata</a></body></head>";
				ps.print(response);
				ps.flush();
				return;
			}

			ps.print(response);
			ps.flush();
			return;
		}
		else if (function.startsWith("dostupan")) {
			String response = dostupan(resources[1]);
			ps.print(response);
			ps.flush();
			return;
		}
		
        File file = new File(resource);

        if (!file.exists())
        {
            // ako datoteka ne postoji, vratimo kod za gresku
            String errorCode = "HTTP/1.0 404 File not found\r\n"
                    + "Content-type: text/html; charset=UTF-8\r\n\r\n<b>404 Not found:"
                    + file.getName() + "</b>";

            ps.print(errorCode);

//            ps.flush();
            System.out.println("Could not find resource: " + file);
            return;
        }

        // ispisemo zaglavlje HTTP odgovora
        ps.print("HTTP/1.0 200 OK\r\n\r\n");

        // a, zatim datoteku
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[8192];
        int len;

        while ((len = fis.read(data)) != -1)
            ps.write(data, 0, len);

        ps.flush();
        fis.close();
    }


	private String pretraga(String param) {
		String grad = param.split("[=]")[1];
		List<Stan> pronadjeniStanovi = new ArrayList<Stan>();

		for (Stan s : stanovi) {
			if (s.getGrad().equalsIgnoreCase(grad)) {
				pronadjeniStanovi.add(s);
			}
		}
		if (pronadjeniStanovi.size() == 0)
			return null;

		return listingStanova(pronadjeniStanovi, false, "Pretraga");
	}


	private String dostupan(String param) {
		String brStana = param.split("[=]")[1];

		for (Stan s : stanovi) {
			if (s.getBrStana().equalsIgnoreCase(brStana)) {
				s.setDostupan(true);
				System.out.println("promenio status");
				break;
			}
		}
		return listingStanova(stanovi, true, "Pregled");
	}


	private String dodaj(String param) {
		String params[] = param.split("[&]");
		String brStana = params[0].split("[=]")[1];

		if (!checkBrStana(brStana))
			return null;

		String adresa = params[1].split("[=]")[1];
		String grad = params[2].split("[=]")[1];
		int brSoba = Integer.parseInt(params[3].split("[=]")[1]);
		int brKvadrata = Integer.parseInt(params[4].split("[=]")[1]);
		double cena;
		boolean centralno = false;
		if (params.length == 6) {
			cena = Double.parseDouble(params[5].split("[=]")[1]);
		}
		else {
			centralno = true;
			cena = Double.parseDouble(params[6].split("[=]")[1]);
		}
		
		Stan s = new Stan(brStana, adresa, grad, brSoba, brKvadrata, centralno, cena);
		stanovi.add(s);

		return listingStanova(stanovi, true, "Pregled");
	}


	private String listingStanova(List<Stan> stanoviZaPregled, boolean pretragaForm, String method) {
		String response = "HTTP/1.0 200 OK\n" + "Content-type: text/html; charset=UTF-8\r\n\r\n"
				+ "<html><head><title>Pregled stanova</title></head>";

		response += "<body>" + "<h3 style=\"color: green;\">HTTP." + method + " stanova</h3><br>"
				+ "<table border='1px'>"
				+ "<tr><th><b>Broj stana</th><th>Adresa</th><th>Grad</th>"
				+ "<th>Broj soba</th><th>Broj kvadrata</th><th>Centralno grejanje</th><th>Cena[EUR]</th><th></th></tr>";

		for (Stan s : stanoviZaPregled) {
			response += "<tr";
			if (s.isDostupan()) {
				response += " style=\"background-color:grey\"";
			}
			response += "><td>" + s.getBrStana() + "</td>" + "<td>" + s.getAdresa() + "</td>"
					+ "<td>" + s.getGrad() + "</td>" + "<td>" + s.getBrSoba() + "</td>" + "<td>"
					+ s.getBrKvadrata() + "</td>" + "<td>" + s.isCentralnoGrejanje() + "</td>"
					+ "<td>" + s.getCena() + "</td>"
					+ "<td>";
			if (!s.isDostupan()) {
				response += "<a href=\"http://localhost:8000/dostupan?brStana=" + s.getBrStana() + "\">Dostupan</a>";
			}
			response += "</td></tr>";
		}
		response += "</table><br>";
		
		if (pretragaForm) {
			response += "<h4>Pretraga stana po gradu:</h4>"
					+ "<form action=\"http://localhost:8000/pretraga\" method=\"GET\">"
					+ "Unesite grad: <input type=\"text\" name=\"grad\"/><br>"
					+ "<input type=\"submit\" value=\"Pretrazi\"/>"
					+ "</form>";
		}
		response += "<br><a href=\"http://localhost:8000/\">Nazad na prijavu pacijanata</a></body></head></html>\n";
		return response;
	}


	private boolean checkBrStana(String brStana) {
		for (Stan s: stanovi) {
			if (s.getBrStana().equalsIgnoreCase(brStana)) {
				return false;
			}
		}
		return true;
	}


}
