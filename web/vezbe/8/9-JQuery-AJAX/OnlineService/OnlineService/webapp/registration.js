$(document).ready(function(){

	let inputElem = $("input[name=ime]");
	let doneIme = false;
	let forma = $("form");

	forma.submit(function (event) {
		let ime = inputElem.val();
		let allTds = $('td');
		if (ime == '' & !doneIme) {
			doneIme = true;
			allTds[1].append("Unesite ime.");
			$(allTds[0]).css("color", "red");
		}
		
		let data = {
			"firstName": $("input[name=ime]").val(),
			"lastName": $("input[name=prezime]").val(),
			"username": $("input[name=korisnickoIme]").val(),
			"password": $("input[name=lozinka]").val(),
			"email": $("input[name=email]").val(),
			"phoneNumber": $("input[name=brojTelefona]").val()
		};

		$.post("/OnlineService/UserServlet",
			JSON.stringify(data),
			function (data, status) {
				console.log(data);
				console.log(status);
				alert("Korisnik " + $("input[name=korisnickoIme]").val() + " uspesno registrovan!");
			}
		);
		event.preventDefault();
	})

});