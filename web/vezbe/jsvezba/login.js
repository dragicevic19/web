var usersMap = {
    "pera":"pera"
};

window.addEventListener('load', function () {
    let forma = this.document.forms[0]; 

    forma.addEventListener('submit', function (event) {
        let valid = false;

        let ime = document.getElementById('ime').value;
        let lozinka = document.getElementById('lozinka').value;

        if (ime in usersMap) {
            valid = usersMap[ime] == lozinka;

        }
        if (valid) {
            return;
        }

        document.getElementById('poruka').innerText = "Neispravno korisnicko ime/lozinka";
        event.preventDefault();
    });
});