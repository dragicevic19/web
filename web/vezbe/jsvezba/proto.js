function Korisnik(ime, prezime, lozinka) {
    this.ime = ime;
    this.prezime = prezime;
    this.lozinka = lozinka;
};

Korisnik.prototype.punoIme = function () {
    return this.ime + " " + this.prezime;
};

function MaloprodajniKorisnik(ime, prezime, lozinka) {
    Korisnik.call(this, ime, prezime, lozinka);
};

MaloprodajniKorisnik.prototype = Object.create(Korisnik.prototype);
MaloprodajniKorisnik.prototype.constructor = MaloprodajniKorisnik;

MaloprodajniKorisnik.prototype.punoIme = function () {
    return "Maloprodajni korisnik: " + Korisnik.prototype.punoIme.call(this);

}

window.addEventListener('load', function () {
    var korisnik1 = new Korisnik("Pera", "Peric", "pera");

    var korisnik2 = new MaloprodajniKorisnik("Mika", "Mikic", "mika");

    this.alert("Moje ime je " + korisnik1.ime);
    this.alert("Moje ime je " + korisnik2.ime);
    alert("Puno ime prvog korisnika je " + korisnik1.punoIme())
    alert("Puno ime drugog korisnika je " + korisnik2.punoIme())

});