<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="beans.Product"%>
<%@page contentType="text/html; charset=UTF-8" %>
<jsp:useBean id="products" class="dao.ProductDAO" scope="application"/>
<!-- Koristi objekat registrovan pod ključem "user" iz sesije -->
<jsp:useBean id="user" class="beans.User" scope="session"/>
<html>
<head>
</head>
<body>
	<p>Dobrodošli, ${user.firstName} <a href="LogoutServlet">Logout</a></p>
	<table border="1">
		<thead>
			<th>Naziv</th>
			<th>Cena</th>
			<th colspan="2">Količina</th>
		</thead>
		<tbody>
		<!-- TODO 3: Izlistavanje proizvoda -->
			<c:forEach var="product" items="${products.findAll()}">
				<tr>
					<td>${product.name}</td>
					<td>${product.price}</td>
					<td>1</td>
					<td><a href="AddToShoppingCartServlet?id=${product.id}">Dodaj u korpu</a></td>
				</tr>
			</c:forEach>
			
		</tbody>
	</table>
	<br>
	<a href="JSP/shoppingCart.jsp">Prikaz proizvoda iz korpe</a>
	
	<c:if test="${not empty log}">
		<p style="color:green">${log}</p>
	</c:if>
	
</body>
</html>