<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="beans.Product"%>
<%@page contentType="text/html; charset=UTF-8" %>
<jsp:useBean id="shoppingCart" class="beans.ShoppingCart" scope="session"/>
<!DOCTYPE html>
<html>
<head>
<title>Korpa</title>
</head>
<body>
	<table border="1">
		<thead>
			<th>Naziv</th>
			<th>Cena</th>
			<th colspan="2">Količina</th>
		</thead>
		<tbody>
		<!-- TODO 3: Izlistavanje proizvoda -->
			<c:forEach var="item" items="${shoppingCart.getItems()}">
				<tr>
					<td>${item.product.name}</td>
					<td>${item.product.price}</td>
					<td>${item.count}</td>
					<td><a href="../RemoveFromShoppingCartServlet?id=${item.product.id}">Izbaci iz korpe</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<br>
	<b>Total: ${shoppingCart.getTotal()}</b><br>
	<a href="../ProductServlet">Nazad</a>
</body>
</html>