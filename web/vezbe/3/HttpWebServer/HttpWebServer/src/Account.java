public class Account {

    private String username;
    private String password;

    public Account(String user, String pass)
    {
        this.username = user;
        this.password = pass;
    }

    public String getUsername(){
        return this.username;
    }

    public String getPassword() {
        return password;
    }
}
