// Ako funkcija vrati true, validacija je uspesna i forma salje podatke
// Ako funkcija vrati false, validacija neije uspaesna i browser ostaje na trenutnoj stranici

function validacija(forma) {
    let imeEl = document.getElementsByName('ime')[0];
    let ime = imeEl.value;
    let prezime = document.getElementsByName('prezime')[0].value;
    let jmbg = document.getElementsByName('jmbg')[0].value;
    let marka = document.getElementsByName('marka')[0].value;
    let tip = document.getElementsByName('tip')[0].value;
    let registracija = document.getElementsByName('registracija')[0].value;

    if (!ime) {
        imeEl.style.background = 'red';
        alert('Forma ne sme imati prazna polja!');
        return false;
    }

    if (!ime || !prezime || !jmbg || marka === '1' || tip === '1' || !registracija) {
        alert('Forma ne sme imati prazna polja');
        return false;
    }

    if (jmbg.length != 13) {
        alert('JMBG mora da sadrzi 13 cifara');
        return false
    }

    if (ime[0] != ime[0].toUpperCase() || prezime[0] != prezime[0].toUpperCase()) {
        alert('Ime i prezime moraju poceti velikim slovom');
        return false;
    }

    return true;
}

function proveraTipa(e) {
    let tipEl = document.getElementsByName('tip')[0];
    let tip = tipEl.value;

    if (tip == "2") {
        let paragraph = document.createElement('p');

        let tabela = document.getElementsByTagName('table')[0];
        let kubikazaTr = document.createElement('tr');
        let labelTd = document.createElement('td');
        let inputTd = document.createElement('td');
        
        labelTd.appendChild(document.createTextNode("Kubikaza"));
        let inputIn = document.createElement('input')
        inputIn.type = "text";
        inputIn.name = "kubikaza";
        inputIn.onkeydown = proveraEkoKlase(this, inputIn, paragraph);
        inputTd.appendChild(inputIn);

        kubikazaTr.appendChild(labelTd);
        kubikazaTr.appendChild(inputTd);

        tabela.appendChild(kubikazaTr);

        paragraph.id = "ekoklasa";
        paragraph.appendChild(document.createTextNode("EKO KLASA"));
        document.getElementsByTagName('body')[0].appendChild(paragraph);
    }

}

function proveraEkoKlase(e, kubikazaIn, paragraph) {
    alert('op');
    if (kubikazaIn.value < 600) {
        paragraph.style.color = 'green';
    }
    else if (kubikazaIn.value <= 1200) {
        paragraph.style.color = 'yellow';
    }
    else {
        paragraph.style.color = 'red';
    }
}