window.onload = function (event) {
    // Lista: [objekat1, objekat2 ...]
    // Objekat : {'polje': vrednost}

    let users = [
        { 'ime': 'Petar', 'prezime': 'Petrovic', 'jmbg': '123321' },
        { 'ime': 'Marko', 'prezime': 'Markovic', 'jmbg': '111111' },
        { 'ime': 'Stevan', 'prezime': 'Stevanovic', 'jmbg': '123456' },
        { 'ime': 'Stevan', 'prezime': 'Stevanovic', 'jmbg': '123456' }
    ];
    
    let tabela = document.getElementsByTagName('table')[0];
    let params = new URLSearchParams(window.location.search);
    let ime = params.get('ime');
    let prezime = params.get('prezime');
    let jmbg = params.get('jmbg');

    users.push(
        { 'ime': ime, 'prezime': prezime, 'jmbg': jmbg },
    )
    for (user of users) {
        // Kreiraj novi red (tr)
        let userTr = document.createElement('tr');
        // kreiraj polja reda (td)
        let imeTd = document.createElement('td');
        let prezimeTd = document.createElement('td');
        let jmbgTd = document.createElement('td');

        // Dodaj tekst u svako polje
        imeTd.appendChild(document.createTextNode(user.ime));
        prezimeTd.appendChild(document.createTextNode(user.prezime));
        jmbgTd.appendChild(document.createTextNode(user.jmbg));

        // Dodaj polja u red
        userTr.appendChild(imeTd);
        userTr.appendChild(prezimeTd);
        userTr.appendChild(jmbgTd);

        // Dodaj red u tabelu
        tabela.appendChild(userTr);
    }
}