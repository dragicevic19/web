function Korisnik(ime, prezime, lozinka) {
    this.ime = ime;
    this.prezime = prezime;
    this.lozinka = lozinka;
};

Korisnik.prototype.ispis = function (tekst) {
    var p = document.querySelector('#poruka');
    p.innerText = tekst;
};

Korisnik.prototype.stampaj = function () {
    Korisnik.prototype.ispis.call(this, 'Hello ' + this.ime + '!');
};

function MaloprodajniKorisnik(ime, prezime) {
    Korisnik.call(this, ime, prezime);
};

MaloprodajniKorisnik.prototype = Object.create(Korisnik.prototype);
MaloprodajniKorisnik.prototype.constructor = MaloprodajniKorisnik;

MaloprodajniKorisnik.prototype.ispis = function (input) {
    let vrednost = 'TODO';
    let poruka = 'Cena narudzbine je: ' + vrednost;
    Korisnik.prototype.ispis.call(this, poruka);
};


function VeleprodajniKorisnik(ime, prezime) {
    Korisnik.call(this, ime, prezime);
}
VeleprodajniKorisnik.prototype = Object.create(Korisnik.prototype);
VeleprodajniKorisnik.prototype.constructor = VeleprodajniKorisnik;