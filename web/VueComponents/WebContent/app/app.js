var global = {count : 0};

var app = new Vue({ 
    el: '#parent',
    data: {
    	fromChild1 : 'child1 message placeholder'
    },
	mounted() {
		this.$root.$on('messageToParent', (text) => {
			this.fromChild1 = text + ', count : ' + global.count;
		});
	},
    methods: {
    	sendToChild1: function() {
    		global.count++;
    		this.$root.$emit('messageFromParent', 'Hello my child');
    	}
    } 
});