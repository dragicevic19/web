<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSP Prijava pacijenta</title>
</head>
<body>
	<h1 style="color: lightblue;">JSP.Prijava pacijenata [COVID-19]</h1>
		<form action="../DodajPacijenta" method="POST" style="align-items: center;">
			<table>
				<tr>
					<td><b>Broj zdravstvenog osiguranja:</b></td><td><input type="text" name="brOsig"/></td>
				</tr>
				<tr>
					<td><b>Ime pacijenta:</td><td><input type="text" name="ime"/></td>
				</tr>
				<tr>
					<td><b>Prezime pacijenta:</td><td><input type="text" name="prezime"/></td>
				</tr>
				<tr>
					<td><b>Datum rodjenja:</td><td><input type="text" name="datumRodj" placeholder="dd/mm/yyyy"/></td>
				</tr>
				<tr>
					<td><b>Pol:</td><td><select name="pol" id="polovi">
										<option value="MUSKI">muski</option>
										<option value="ZENSKI">zenski</option>
									</select></td>
				</tr>
				<tr>
					<td><b>Zdravstveni status:</td><td><select name="zStatus" id="zStatus">
											<option value="BEZ_SIMPTOMA">BEZ SIMPTOMA</option>
											<option value="SA_SIMPTOMIMA">SA SIMPTOMIMA</option>
											<option value="ZARAZEN">ZARAZEN</option>
											<option value="IZLECEN">IZLECEN</option>
										</select></td>
				</tr>
				<tr>
					<td></td><td><input type="submit" value="posalji"></td>
				</tr>
			</table>
		</form>
		<!-- Prikaži grešku, ako je bilo -->
		<c:if test="${not empty err}">
			<p style="color:red">${err}</p>
		</c:if>
</body>
</html>