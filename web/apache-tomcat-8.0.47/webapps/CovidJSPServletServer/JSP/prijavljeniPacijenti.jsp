<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="model.Pacijent"%>
<%@page import="model.ZdravStatus"%>
<jsp:useBean id="pacijenti" class="dao.PacijentDAO" scope="application"/>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Pregled pacijenata</title>
</head>
<body>
	<h2 style="color: lightblue;">JSP.Pregled pacijenata [COVID-19]</h2>
	
	<table border='1px'>
		<tr>
			<th><b>Broj zdravstvenog osiguranja</b></th>
			<th><b>Ime pacijenta</b></th>
			<th><b>Prezime pacijenta</b></th>
			<th><b>Datum rodjenja</b></th>
			<th><b>Pol</b></th>
			<th><b>Zdravstveni status</b></th>
			<th></th>
		</tr>
		<c:forEach var="pacijent" items="${pacijenti.findAll()}">
			<c:choose>
				<c:when test="${pacijent.zdravStatus == ZdravStatus.BEZ_SIMPTOMA}">
					<tr style="background-color:lightblue;">
						<td>${pacijent.brOsiguranja}</td>
						<td>${pacijent.ime}</td>
						<td>${pacijent.prezime}</td>
						<td>${pacijent.datumRodjenja}</td>
						<td>${pacijent.pol}</td>
						<td>${pacijent.zdravStatus}</td>
						<td>
							<c:choose>
								<c:when test="${pacijent.zdravStatus != ZdravStatus.ZARAZEN}">
									<a href="../Pozitivan?brOsiguranja=${pacijent.brOsiguranja}">Test je pozitivan</a>
								</c:when>	
								<c:otherwise></c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:when>
				<c:otherwise>
					<tr>
						<td>${pacijent.brOsiguranja}</td>
						<td>${pacijent.ime}</td>
						<td>${pacijent.prezime}</td>
						<td>${pacijent.datumRodjenja}</td>
						<td>${pacijent.pol}</td>
						<td>${pacijent.zdravStatus}</td>
						<td>
							<c:choose>
								<c:when test="${pacijent.zdravStatus != ZdravStatus.ZARAZEN}">
									<a href="../Pozitivan?brOsiguranja=${pacijent.brOsiguranja}">Test je pozitivan</a>
								</c:when>	
								<c:otherwise></c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</c:forEach>	
	</table>
</body>
</html>